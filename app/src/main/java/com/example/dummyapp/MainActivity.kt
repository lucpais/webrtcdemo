package com.example.dummyapp

import android.Manifest
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.telnyx.webrtc.sdk.Call
import com.telnyx.webrtc.sdk.CredentialConfig
import com.telnyx.webrtc.sdk.TelnyxClient
import com.telnyx.webrtc.sdk.model.CallState
import com.telnyx.webrtc.sdk.model.LogLevel
import com.telnyx.webrtc.sdk.model.SocketMethod
import com.telnyx.webrtc.sdk.verto.receive.*
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.util.*

class MainActivity : AppCompatActivity() {

    private var telnyxClient: TelnyxClient? = null
    private var calls: Map<UUID, Call> = mapOf()
    private var currentCall: Call? = null
    private var previousCall: Call? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tv_status.text = "SOCKET NOT ESTABLISHED"
        tv_login.text = "USER NOT LOGGED"

        btn_call.isEnabled = false
        btn_end_call.isEnabled = false

        btn_reject.isEnabled = false
        btn_answer.isEnabled = false

        btn_end_call.setOnClickListener {
            endCurrentCall()
        }
        btn_call.setOnClickListener {
            startCall()
        }

        hold_button_id.setOnClickListener {
            currentCall?.callId?.let { currentCall?.onHoldUnholdPressed(it) }
        }
        mute_button_id.setOnClickListener {
            currentCall?.onMuteUnmutePressed()
        }
        speaker_button_id.setOnClickListener {
            currentCall?.onLoudSpeakerPressed()
        }

        checkPermissions()
    }

    private fun endCurrentCall() {
        currentCall?.callId?.let { telnyxClient?.call?.endCall(it) }
        previousCall?.let { currentCall = it }
        if (telnyxClient?.getActiveCalls()?.isEmpty() == true) {
            btn_end_call.isEnabled = false
        }
    }

    private fun startCall() {
        telnyxClient?.call?.newInvite(
            callerName = "Lucas Caller Name",
            callerNumber = "+1 999999999",
            destinationNumber = "+17733028060",
            clientState = "Sample Client State"
        )
    }

    private fun connectToSocketAndObserve() {
        Timber.d("REQUIRE CLIENT CONNECTION")
        telnyxClient = TelnyxClient(applicationContext)
        telnyxClient?.connect()
        observeSocketResponses()
    }

    private fun initiateLogin() {
        //checkPermissions()

        val sipUsername = "lucasp"
        val sipPassword = "luc4spP4ssw0rd.!"
        val sipCallerName = "LucasP calling.."
        val sipCallerNumber = "LucasP number.."

        val credentialConfig = CredentialConfig(
            sipUser = sipUsername,
            sipPassword = sipPassword,
            sipCallerIDName = sipCallerName,
            sipCallerIDNumber = sipCallerNumber,
            fcmToken = null,
            ringtone = null,
            ringBackTone = null,
            logLevel = LogLevel.ALL
        )

        telnyxClient?.credentialLogin(credentialConfig)
        tv_login.text = "LOGIN STARTING.."

    }

    private fun observeSocketResponses() {
        telnyxClient?.getSocketResponse()
            ?.observe(this, object : SocketObserver<ReceivedMessageBody>() {
                override fun onConnectionEstablished(data: ReceivedMessageBody?) {
                    Timber.d("onConnectionEstablished Dummy: " + data?.method)
                    tv_status.text = "SOCKET CONNECTED"
                    initiateLogin()
                }

                override fun onMessageReceived(data: ReceivedMessageBody?) {
                    Timber.d("onMessageReceived from SDK [%s]", data?.method)
                    when (data?.method) {
                        SocketMethod.LOGIN.methodName -> {
                            val sessionId = (data.result as LoginResponse).sessid
                            Timber.d("Current Session: $sessionId")
                            tv_login.text = "LOGGED"
                            btn_call.isEnabled = true
                        }

                        SocketMethod.ANSWER.methodName -> {
                            val callId = (data.result as AnswerResponse).callId
                            launchCallInstance(callId)
                        }

                        SocketMethod.INVITE.methodName -> {
                            val inviteResponse = data.result as InviteResponse
                            onReceiveCallView(
                                inviteResponse.callId,
                                inviteResponse.callerIdName,
                                inviteResponse.callerIdNumber
                            )
                        }

                    }
                }

                override fun onLoading() {
                    tv_status.text = "SOCKET ESTABLISHING..."
                }

                override fun onError(message: String?) {
                    tv_status.text = "SOCKET CONNECTION FAILED!: $message"
                }
            })

    }

    fun observeCallParms() {
        currentCall?.getCallState()?.observe(this, {
            call_status.text = it.name
            when (it) {
                CallState.ACTIVE -> active_call_layout.visibility = View.VISIBLE
                CallState.DONE -> active_call_layout.visibility = View.GONE
            }
        })

        currentCall?.getIsMuteStatus()?.observe(this, { isMuted ->
            if (isMuted) {
                mute_button_id.setImageResource(R.drawable.ic_mic)
            } else {
                mute_button_id.setImageResource(R.drawable.ic_mic_off)
            }
        })

        currentCall?.getIsOnHoldStatus()?.observe(this, { isHolded ->
            if (isHolded) {
                hold_button_id.setImageResource(R.drawable.ic_play)
            } else {
                hold_button_id.setImageResource(R.drawable.ic_hold)
            }
        })

        currentCall?.getIsOnLoudSpeakerStatus()?.observe(this, { isLoud ->
            if (isLoud) {
                speaker_button_id.setImageResource(R.drawable.ic_loud_speaker_off)
            } else {
                speaker_button_id.setImageResource(R.drawable.ic_loud_speaker)
            }
        })
    }

    private fun launchCallInstance(callId: UUID) {
        setCurrentCall(callId)
        btn_end_call.isEnabled = true
    }

    private fun onReceiveCallView(callId: UUID, callerIdName: String, callerIdNumber: String) {
        setCurrentCall(callId)

        btn_answer.isEnabled = true
        btn_reject.isEnabled = true

        btn_answer.setOnClickListener {
            telnyxClient?.call?.acceptCall(callId, callerIdNumber)
            setCurrentCall(callId)
            btn_reject.isEnabled = false
            btn_answer.isEnabled = false
            btn_end_call.isEnabled = true
        }
        btn_reject.setOnClickListener {
            btn_answer.isEnabled = false
            telnyxClient?.call?.endCall(callId)
            btn_reject.isEnabled = false
        }
    }

    private fun setCurrentCall(callId: UUID) {
        calls = telnyxClient?.getActiveCalls()!!
        if (calls.size > 1) {
            previousCall = currentCall
        }
        currentCall = calls[callId]
        observeCallParms()
    }


    private fun checkPermissions() {
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.INTERNET,
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report!!.areAllPermissionsGranted()) {
                        connectToSocketAndObserve()
                    } else if (report.isAnyPermissionPermanentlyDenied) {
                        Toast.makeText(
                            this@MainActivity,
                            "Audio permissions are required to continue",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            }).check()
    }

}